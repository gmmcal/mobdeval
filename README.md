# Desenvolvimento mobile colaborativo: Como desenvolver mobile utilizando versionamento Git

Palestra submetida ao [MobDev/AL](http://www.ic.ufal.br/evento/mobdev/) sobre desenvolvimento de sistemas mobile colaborativo utilizando versionamento Git.

Criada colaborativamente por [Gustavo Cunha](http://www.gmmcal.com.br) e [Ivan Santos](http://www.ivansjr.com.br), foi utilizado o framework de apresentações HTML [reveal.js](http://lab.hakim.se/reveal-js/).
